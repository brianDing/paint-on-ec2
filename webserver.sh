#!/bin/bash

if (( $# > 0 ))
then
    hostname=$1
else
    echo "Please supply a hostname or IP address as an argument" 1>&2
    exit 1
fi

scp -o StrictHostKeyChecking=no  -i ~/.ssh/BrianTingKey.pem jspaint.init ec2-user@$hostname:jspaint.init 

ssh -o StrictHostKeyChecking=no  -i ~/.ssh/BrianTingKey.pem ec2-user@$hostname '
sudo yum -y install git
git clone https://github.com/1j01/jspaint.git

VERSION=v14.16.1
DISTRO=linux-x64
sudo mkdir -p /usr/local/lib/nodejs
wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

echo "
# Nodejs
VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >> ~/.bash_profile
source ~/.bash_profile

sudo yum -y install httpd
sudo sh -c "echo \"<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" > /etc/httpd/conf.d/jspaint.conf"

sudo systemctl start httpd
cd ~/jspaint
npm i
sudo mv ~/jspaint.init /etc/init.d/jspaint 
sudo chmod +x /etc/init.d/jspaint
sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/," /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo /etc/init.d/jspaint start
'