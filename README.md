# Setting up AWS EC2 with JS Paint
*In this project we will make a bash script to provision a machine to run JS Paint*

## Subjects Covered 
- Git (Bit Bucket)
- Markdown (.md)
- AWS EC2 creation on GUI
- EC2 security groups
- Bash 
  - Installation scripts
  - Redhat installation process
  - SSH
  - SCP
  - Heredoc
  - Outputs (stdin, stdout, stderr)
  - Piping and grep command 

---
## **Steps involved to run JSPaint**
1. Install git on EC2 machine
2. git clone JS Paint repo onto machine
3. Install NodeJs
   1. Set declare variables
   2. Download source code of NodeJs 
   3. Run source code to install 
   4. Set environment variables
   5. Refresh profile
4. Create reverse proxy
5. Install package dependencies
6. Run JS Paint

---
# Code snippets explained in plain english
## **PROVIDING IP ADDRESS/ HOSTNAME AS AN ARGUMENT**
```bash
if (( $# > 0 ))
then
    hostname=$1
else
    echo "Please supply a hostname or IP address as an argument" 1>&2
    exit 1
fi
```
- if there are more than 0 arguments when running this bashscript
- then set hostname variable to equal to the first argument 
- otherwise 1>&2 map stdin to stderr to turn output into error msg
- Non 0 exit code to show its an error

---
## **SECURE COPY INIT FILE ONTO EC2 SYSTEM**
```bash
$scp -o StrictHostKeyChecking=no  -i ~/.ssh/BrianTingKey.pem jspaint.init ec2-user@$hostname:jspaint.init 
```
- Secure copy without checking fingerprint, location of pem security key, file to be copied, user@host:home directory with file name jspaint.init
- Could be short-hand written to ec2-user@$hostname: , where no destination implies home directory and no file name implies same file name

---
## **SSH INTO SYSTEM BASED ON HOSTNAME GIVEN AS ARGUMENT**
```bash
ssh -o StrictHostKeyChecking=no  -i ~/.ssh/BrianTingKey.pem ec2-user@$hostname ' lorem '
```

- ssh with options, no strict host key checking so dont want them to ask for approval for the fingerprint
- -i = identification, specifying our pem key
- system user name + EC2 hostname that was inputted as an argument
- Commands wrapped in ' ' will all be ran in that order in the cmd

--- 
## **INSTALL NODEJS IN A BINARY DISTRIBUTIVE WAY**
```bash
VERSION=v14.16.1
DISTRO=linux-x64
sudo mkdir -p /usr/local/lib/nodejs
wget https://nodejs.org/dist/v14.16.1/node-$VERSION-$DISTRO.tar.xz
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

echo "
# Nodejs
VERSION=v10.15.0
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >> ~/.bash_profile
source ~/.bash_profile
```

- Set VERSION and DISTRO variables
- Make a new directory, -p means if the files in the listed directory dont exist make them as well
- wget to download the nodejs source code file pulling the version and distro from the variables
- Run sourcecode file using the variables we just set to correspond to the source code file 
- Append all the lines wrapped in " " into our profile so that it becomes global variables available on login
- export PATH ... writes a new path as well as appending the original path at the end separated by : 
- Decision to put original path before or after new path depending on which commands you want to run as a priority as the commands in path earlier on will run quicker 
- Re-establish our values in the profile so that it is available to our current session

---
## **INSTALLING AND SETTING UP REVERSE PROXY**
```bash
sudo yum -y install httpd
sudo sh -c "echo 
'<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>' > jspaint.conf"
```

- Install httpd, an Apache server 
- Write into jspaint.conf file (creating it if it does not exist) reverse proxy setup
- Sudo does not like things being redirected so sh -c runs a shell (born shell which is the most basic shell available) running a command (-c) to allow the redirect
- XML explanation: 
  - Port 80 commonly used for HTTP
  - proxypass -> Any requests going to / or the root  (e.g. http://34.247.91.100) then redirect it to 8080 (where JSPaint is running)
  - 


---
## **Init file -> /etc/init.d/jspaint**
```bash
#!/bin/bash

#description: JSPaint web app
#chkconfig: 2345 99 99

case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 (ps -ef | egrep 'npm|node' | awk'{print $2}')
		;;
esac
```

- case checks $1 and match to conditions listed below (start/ stop) and depending on which one, it will run the cmd listed inside, ;; stops flow through beyond that point
- nohup = no hang up, run the command regardless
- & = run process in background 
- kill -9 = kill everything , ps -ef looks for all processes with npm and kill it
- chkconfig: 2345 99 99 -> Make this file available at boot levels 2-5(all typical boot modes), when to start (99 i.e. last thing to start) and when to stop (99 i.e. first process to stop)
- egrep (extended grep) check for node or npm (' ' stops shell from running it as a pipe instead use it as an or for egrep)
- awk extracts output looking for only the second column which is the processID, ' ' similar function to in egrep


---
## **PROCESSING INIT FILE**
```bash
sudo mv ~/jspaint.init /etc/init.d/jspaint 
sudo chmod +x /etc/init.d/jspaint
sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/," /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo /etc/init.d/jspaint start
```
- Move the jspaint init file to the init folder and rename it to without .init
- Turn the jspaint into an executable
- Using sed (stream) find and replace (-i, s,) the path variable into the full node path 
- \$PATH is to stop it from being substituted 
- Add the jspaint init script into the chkconfig process
- System control enable the jspaint init script
- Start JSPaint through the start command from the init script 
- *Would normally use systemctl to run it but not working in this example*